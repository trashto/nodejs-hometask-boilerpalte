const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    getFighters() {
        return FighterRepository.getAll();
    }

    saveFighter(data) {
        if(this.search({name: data.name})){
            throw 'Name already taken';
        }
        if (!data.health){
            data.health = 100;
        }
        const newFighter = FighterRepository.create(data);
        if(!newFighter){
            return null;
        }
        return newFighter;
    }

    updateFighter(id, data) {
        if(data.name && !this.search({name: data.name})){
            throw 'Name already taken';
        }
        if(!this.search({id: id})){
            return null
        }
        return FighterRepository.update(id, data)
    }

    deleteFighter(id) {
        if(!this.search({id: id})){
            return null;
        }
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();