const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights

    getAllHistory(){
        return FightRepository.getAll();
    }

    search(search){
        const item = FightRepository.getOne(search);
        if(!item){
            return null;
        }
        return item;
    }
    
    saveHistory(history){
        const matchHistory = FightRepository.create(history);
        if(!matchHistory){
            return null;
        }
        return matchHistory;
    }
}

module.exports = new FightersService();