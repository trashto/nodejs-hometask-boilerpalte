const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    getUsers(){
        return UserRepository.getAll();
    }

    saveUser(user){
        if(this.search({email: user.email})){
            throw 'Email already taken';
        }
        if(this.search({phoneNumber: user.phoneNumber})){
            throw 'Phone number already taken';
        }
        const newUser = UserRepository.create(user);
        if(!newUser){
            return null;
        }
        return newUser;
    }
    
    updateUser(id, userData){
        if(userData.email && this.search({email: userData.email})){
            throw 'Email already taken';
        }
        if(userData.phoneNumber && this.search({phoneNumber: userData.phoneNumber})){
            throw 'Phone number already taken';
        }
        if(!this.search({id: id})){
            return null
        }
        return UserRepository.update(id, userData)
    }
    
    deleteUser(id){
        if(!this.search({id: id})){
            return null;
        }
        return UserRepository.delete(id);
    }

}

module.exports = new UserService();