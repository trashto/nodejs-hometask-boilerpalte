const { fighter } = require('../models/fighter');
const { sendError, validateExtraFields } = require('./utils');


const isNumberInRange = (num, min, max) => {
    
    return Number.isInteger(num) && num >= min && num <= max;
}

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if (req.body.id) {
        sendError(res, 'Body must not contain fighter id')
        return;
    }
    if (!req.body.name) {
        sendError(res, 'Fighter name is empty')
        return;
    }
    if (!req.body.power || !isNumberInRange(req.body.power, 1 , 100)) {
        sendError(res, 'Power value is empty or does not between 1 and 100')
        return;

    }
    if (!req.body.defense || !isNumberInRange(req.body.defense, 1 , 10)) {
        sendError(res, 'Defense value is empty or does not between 1 and 10')
        return;
    }
    if (req.body.health && !isNumberInRange(req.body.health, 80 , 120)) {
        sendError(res, 'Health must be from 80 to 120')
        return;
    }
    if (!validateExtraFields(req.body, fighter)) {
        sendError(res, 'Request must not contain extra fields')
        return;
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if (req.body.id) {
        sendError(res, 'Body must not contain fighter id')
        return;
    }

    if (Object.keys(req.body).length < 1) {
        sendError(res, 'Body must contain at least one field')
        return;
    }

    if (req.body.name && !req.body.name.length) {
        sendError(res, 'Fighter name is empty')
        return;
    }

    if (req.body.power && !isNumberInRange(req.body.power, 1 , 100)) {
        sendError(res, 'Power must be from 1 to 100')
        return;

    }

    if (req.body.defense && !isNumberInRange(req.body.defense, 1 , 10)) {
        sendError(res, 'Defense must be from 1 to 10')
        return;
    }

    if (req.body.health && !isNumberInRange(req.body.health, 80 , 120)) {
        sendError(res, 'Health must be from 80 to 120')
        return;
    }
    if (!validateExtraFields(req.body, fighter)) {
        sendError(res, 'Request must not contain extra fields')
        return;
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;