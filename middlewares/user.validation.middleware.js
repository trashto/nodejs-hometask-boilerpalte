const { user } = require('../models/user');
const { sendError, validateExtraFields } = require('./utils');

const validateEmail = (email) => {
    const regex = /^(.+)@gmail(.+)$/;
    return regex.test(email);
}

const validatePhoneNum = (number) => {
    const regex = /^\+?([0-9]{3})\)?[-. ]?([0-9]{9})$/;
    return regex.test(number);
}

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    if (req.body.id) {
        sendError(res, 'Body must not contain user id')
        return;
    }
    if (!req.body.firstName) {
        sendError(res, 'User firts name is empty')
        return;
    }
    if (!req.body.lastName) {
        sendError(res, 'User last name is empty')
        return;
    }
    if (!req.body.password) {
        sendError(res, 'User password is empty')
        return;
    }
    if (req.body.password.length < 3) {
        sendError(res, 'User password is small')
        return;
    }
    if (!validateEmail(req.body.email)) {
        sendError(res, 'User email is invalid')
        return;
    }
    if (!validatePhoneNum(req.body.phoneNumber)) {
        sendError(res, 'User phone number is invalid')
        return;
    }
    if(!validateExtraFields(req.body, user)){
        sendError(res, 'Request must not contain extra fields')
        return;
    }
    next();
}


const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    if (req.body.id) {
        sendError(res, 'Body must not contain user id')
        return;
    }

    if (Object.keys(req.body).length < 1) {
        sendError(res, 'Body must contain at least one field')
        return;
    }

    if (req.body.firstName && !req.body.firstName.length) {
        sendError(res, 'User firts name is empty')
        return;
    }

    if (req.body.lastName && !req.body.lastName.length) {
        sendError(res, 'User last name is empty')
        return;
    }

    if (req.body.password && req.body.password.length < 3) {
        sendError(res, 'User password is empty or incorrect')
        return;
    }

    if (req.body.email && !validateEmail(req.body.email)) {
        sendError(res, 'User email is invalid')
        return;
    }

    if (req.body.phoneNumber && !validatePhoneNum(req.body.phoneNumber)) {
        sendError(res, 'User phone number is invalid')
        return;
    }
    if(!validateExtraFields(req.body, user)){
        sendError(res, 'Request must not contain extra fields')
        return;
    }
    next();
}



exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;