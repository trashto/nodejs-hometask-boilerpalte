const sendError = (res, message) => {
    res.status(400).send({
        error: true,
        message: message
    });
}
const validateExtraFields = (data, model) => {
    const reqFields = Object.keys(data);
    const allowedFields = Object.keys(model);
    return reqFields.every(field => allowedFields.includes(field));
}

exports.sendError = sendError;
exports.validateExtraFields = validateExtraFields;
