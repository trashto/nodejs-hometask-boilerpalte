const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function (req, res, next) {
    const fighters = FighterService.getFighters()
    res.send(fighters)

})

router.get('/:id', function (req, res, next) {
    const fighterInfo = FighterService.search({ id: req.params.id });

    if (fighterInfo) {
        res.send(fighterInfo);
    } else {
        res.status(404).send({
            error: true,
            message: 'Fighter not found'
        });
    }
})
router.post('/', createFighterValid, function (req, res, next) {
    try{
        const newFighter = FighterService.saveFighter(req.body)
        if (newFighter) {
            res.send(newFighter)
        } else {
            res.status(500).send({
                error: true,
                message: 'Something wrong'
            })
        }
    }catch(error){
        res.status(400).send({
            error: true,
            message: error
        })
    }
   
})

router.put('/:id', updateFighterValid, function (req, res, next) {
    try{
        const updateFighterInfo = FighterService.updateFighter(req.params.id, req.body);
        if (updateFighterInfo) {
            res.send(updateFighterInfo);
        } else {
            res.status(404).send({
                error: true,
                message: 'Fighter does not exist'
            });
        }
    }catch(error){
        res.status(400).send({
            error: true,
            message: error
        })
    }
})

router.delete('/:id', function (req, res, next) {
    const removedFighter = FighterService.deleteFighter(req.params.id);
    if (removedFighter) {
        res.send(removedFighter)
    } else {
        res.status(404).send({
            error: true,
            message: 'Fighter does not exist'
        });
    }
})
module.exports = router;