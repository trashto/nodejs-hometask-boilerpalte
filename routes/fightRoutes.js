const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router.get('/', function (req, res, next) {
    const allHistory = FightService.getAllHistory()
    res.send(allHistory);
})

router.get('/:id', function (req, res, next) {
    const matchHistory = FightService.search({ id: req.params.id });

    if (matchHistory){
        res.send(matchHistory);
    } else {
        res.status(400).send({
            error: true,
            message: 'Match not found'
        });
    }
})

router.post('/', function (req, res, next) {
    const newMatch = FightService.saveHistory(req.body);
    if(newMatch){
        res.send(newMatch)
    } else {
        res.status(500).send({
            error: true,
                message: 'Something wrong'
        })
    }
})


module.exports = router;