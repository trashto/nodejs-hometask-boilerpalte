const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.use(responseMiddleware);

router.get('/', function (req, res, next) {
    const users = UserService.getUsers()
    res.send(users)
})

router.get('/:id', function (req, res, next) {
    const userInfo = UserService.search({ id: req.params.id });

    if (userInfo) {
        res.send(userInfo);
    } else {
        res.status(404).send({
            error: true,
            message: 'User not found'
        });
    }
})

router.post('/', createUserValid, function (req, res, next) {
    try{
        const newUser = UserService.saveUser(req.body)
        if (newUser) {
            res.send(newUser)
        } else {
            res.status(500).send({
                error: true,
                message: 'Something wrong'
            })
        }
    }catch(error){
        res.status(400).send({
            error: true,
            message: error
        })
    }
   
})

router.put('/:id', updateUserValid, function (req, res, next) {
    try{
        const updateUserInfo = UserService.updateUser(req.params.id, req.body);
        if (updateUserInfo) {
            res.send(updateUserInfo);
        } else {
            res.status(404).send({
                error: true,
                message: 'User does not exist'
            });
        }
    }catch(error){
        res.status(400).send({
            error: true,
            message: error
        })
    }
})

router.delete('/:id', function (req, res, next) {
    const removeUser = UserService.deleteUser(req.params.id);
    if (removeUser) {
        res.send(removeUser)
    } else {
        res.status(404).send({
            error: true,
            message: 'User does not exist'
        });
    }
})

module.exports = router;